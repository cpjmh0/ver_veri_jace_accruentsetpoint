package com.accruent.setsetpoint.worker;

import javax.baja.sys.NotRunningException;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.util.BWorker;
import javax.baja.util.CoalesceQueue;
import javax.baja.util.Worker;

public class BSetSetPointWorker
    extends BWorker
{
  /*-
   class BSetSetPointWorker
   {
     properties
     {
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.setsetpoint.worker.BSetSetPointWorker(2430219853)1.0$ @*/
/* Generated Wed Aug 02 15:12:07 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BSetSetPointWorker.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  private CoalesceQueue queue;
  private Worker worker;


    @Override
    public Worker getWorker()
    {
      if(null == worker)
      {
        queue = new CoalesceQueue(1000);
        worker = new Worker(queue);
      }
      return worker;
    }

    public void postWork(Runnable r)
    {
      if (null == queue || !isRunning())
      {
        throw new NotRunningException();
      }
      queue.enqueue(r);
    }

}
