package com.accruent.setsetpoint;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.baja.naming.BOrd;
import javax.baja.sys.BComponent;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.accruent.setsetpoint.controlaction.ControlActionTransaction;

public class BSetSetPointResults
    extends BComponent
{
  /*-
   class BSetSetPointResults
   {
     properties
     {
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.setsetpoint.BSetSetPointResults(510038661)1.0$ @*/
/* Generated Wed Aug 02 15:27:22 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BSetSetPointResults.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  ArrayList<BOrd> alBOrd = null;
  ArrayList<ControlActionTransaction> alControlActionTransactions = null;
  Hashtable<String, String> htRemoteChannelIds = null;
  Hashtable<String, String> htErrors = null;
  
  BSetSetPointResults(ArrayList<BOrd> alBOrdp, ArrayList<ControlActionTransaction> alControlActionTransactionsp, Hashtable<String, String> htRemoteChannelIdsp, Hashtable<String, String>htErrorsp)
  {
     this.alBOrd = alBOrdp; 
     this.alControlActionTransactions = alControlActionTransactionsp;
     this.htRemoteChannelIds = htRemoteChannelIdsp;
     this.htErrors = htErrorsp;
  }
  
  public ArrayList<BOrd> getAlBOrd()
  {
    return alBOrd;
  }
  
  public void setAlBOrd(ArrayList<BOrd> alBOrd)
  {
    this.alBOrd = alBOrd;
  }
  public ArrayList<ControlActionTransaction> getAlControlActionTransactions()
  {
    return alControlActionTransactions;
  }
  public void setAlControlActionTransactions(ArrayList<ControlActionTransaction> alControlActionTransactions)
  {
    this.alControlActionTransactions = alControlActionTransactions;
  }
  public Hashtable<String, String> getHtRemoteChannelIds()
  {
    return htRemoteChannelIds;
  }

  public void setHtRemoteChannelIds(Hashtable<String, String> htRemoteChannelIds)
  {
    this.htRemoteChannelIds = htRemoteChannelIds;
  }

  public Hashtable<String, String> getHtErrors()
  {
    return htErrors;
  }

  public void setHtErrors(Hashtable<String, String> htErrors)
  {
    this.htErrors = htErrors;
  }

}
