package com.accruent.setsetpoint;

import javax.baja.sys.BComponent;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.web.WebOp;

public class BSetSetPointValues
    extends BComponent
{
  /*-
   class BSetSetPointValues
   {
     properties
     {
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.setsetpoint.BSetSetPointValues(3964278966)1.0$ @*/
/* Generated Wed Aug 02 15:27:22 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BSetSetPointValues.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

private WebOp webop = null;


public BSetSetPointValues()
{
}

public BSetSetPointValues(WebOp webOpp)
{
     this.webop = webOpp;

}

public WebOp getWebop()
{
  return webop;
}

public void setWebop(WebOp webop)
{
  this.webop = webop;
}
}
