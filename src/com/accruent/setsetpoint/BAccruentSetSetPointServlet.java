package com.accruent.setsetpoint;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.control.BBooleanWritable;
import javax.baja.control.BEnumWritable;
import javax.baja.control.BNumericWritable;
import javax.baja.control.BStringWritable;
import javax.baja.control.util.BNumericOverride;
import javax.baja.control.util.BStringOverride;
import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.naming.BOrd;
import javax.baja.sys.Action;
import javax.baja.sys.BBoolean;
import javax.baja.sys.BComponent;
import javax.baja.sys.BValue;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;

import com.accruent.setsetpoint.controlaction.ControlActionInfo;
import com.accruent.setsetpoint.controlaction.ControlActionTransaction;
import com.google.gson.Gson;

public class BAccruentSetSetPointServlet
    extends BWebServlet
{
  /*-
   class BAccruentSetSetPointServlet
   {
     properties
     {
         servletName : String
         default {[new String()]}
     }
     actions
     {
        configureModule() : BValue
        flags {summary}

     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.setsetpoint.BAccruentSetSetPointServlet(1059786273)1.0$ @*/
/* Generated Thu Jul 04 12:37:51 CDT 2019 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "servletName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>servletName</code> property.
   * @see com.accruent.setsetpoint.BAccruentSetSetPointServlet#getServletName
   * @see com.accruent.setsetpoint.BAccruentSetSetPointServlet#setServletName
   */
  public static final Property servletName = newProperty(0, new String(),null);
  
  /**
   * Get the <code>servletName</code> property.
   * @see com.accruent.setsetpoint.BAccruentSetSetPointServlet#servletName
   */
  public String getServletName() { return getString(servletName); }
  
  /**
   * Set the <code>servletName</code> property.
   * @see com.accruent.setsetpoint.BAccruentSetSetPointServlet#servletName
   */
  public void setServletName(String v) { setString(servletName,v,null); }

////////////////////////////////////////////////////////////////
// Action "configureModule"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>configureModule</code> action.
   * @see com.accruent.setsetpoint.BAccruentSetSetPointServlet#configureModule()
   */
  public static final Action configureModule = newAction(Flags.SUMMARY,null);
  
  /**
   * Invoke the <code>configureModule</code> action.
   * @see com.accruent.setsetpoint.BAccruentSetSetPointServlet#configureModule
   */
  public BValue configureModule() { return (BValue)invoke(configureModule,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentSetSetPointServlet.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

static Logger logger = Logger.getLogger("BAccruentSetSetPointServlet.class");
static String SEPARATOR = " : ";
  
ArrayList<String> alDefaultUnitsOfMeasure = new ArrayList<String>();  

public BValue doConfigureModule()
{
  BBoolean isConfigured = BBoolean.make("false");
  BufferedReader br = null;
  BIFile fcontrolActionsConfig = BFileSystem.INSTANCE.findFile(new FilePath("^AccruentData/config/controlActionConfig.ini"));
  String inRec = null;
  String key = null;
  String value = null;
  logger.log(Level.INFO, String.format("*****Configuring the SendSetPointInfo Module******", ""));
  if(null != fcontrolActionsConfig)
  {
    try
    {
       br = new BufferedReader(new InputStreamReader(fcontrolActionsConfig.getInputStream()));
       while(br.ready())
       {
         inRec = br.readLine();
         if(inRec.contains(SEPARATOR))
         {
           key = inRec.split(SEPARATOR)[0];
           value = inRec.split(SEPARATOR)[1];
           logger.log(Level.INFO,String.format("Key = %s  Value = %s", key, value));
         }
         if(key.equals("SETPOINT_URL"))
         {
             isConfigured = BBoolean.make("true");
         }
       }
       if(isConfigured.getBoolean())
       {  
          setEnabled(Boolean.TRUE);
          this.start();
          Sys.getStation().save();
       }   
       br.close();
    }
    catch(Exception e)
    {
      logger.log(Level.SEVERE, String.format("Error Configuring Accruent Alarm Module %s", e.getMessage()));
    }
  }
  else
  {
    logger.log(Level.INFO, String.format("Could not get the file to configure the service" ));
    this.setEnabled(false);
    Sys.getStation().save();
    this.stop();
  }
  return isConfigured;
}



@SuppressWarnings("unchecked")
@Override
public void doPost(WebOp webOp)
{
  String timeOut = webOp.getRequest().getParameter("timeout");
  Gson gson = new Gson();
  JaceResults jaceResults = null;
  setAlDefaultUnitsOfMeasure(gson.fromJson(webOp.getRequest().getParameter("defaultUnitOfMeasure"), alDefaultUnitsOfMeasure.getClass()));
  BSetSetPointResults bss = doSetSetPoint(new BSetSetPointValues(webOp));

  try
  {
    Thread.sleep(Integer.valueOf(timeOut) * 1000);
  }
  catch(Exception e)
  {
    e.printStackTrace();
  }
  
  if (null != bss)
  {
    jaceResults = validateResults(bss);
  }
  try
  {
    DataOutputStream dos = new DataOutputStream(webOp.getResponse().getOutputStream());
    dos.write(String.format("validatedResults=%s",gson.toJson(jaceResults, jaceResults.getClass())).getBytes());
    dos.flush();
    dos.close();
  }
  catch(Exception e)
  {
    e.printStackTrace();
  }
}

private JaceResults validateResults(BSetSetPointResults bss)
{
  ArrayList<ValidatedResults> alValidatedResults = new ArrayList<ValidatedResults>();
  String ergDeviceId = null;
  String key = null;
  BOrd bOrd = null;
  BComponent bPoint = null;
  ControlActionTransaction cat = null;
  if (null != bss)
  {
    if (null != bss.getHtRemoteChannelIds())
    {
      for (Enumeration<String> keys = bss.getHtRemoteChannelIds().keys(); keys.hasMoreElements();)
      {
        try
        {
        key = keys.nextElement();
        ergDeviceId = bss.getHtRemoteChannelIds().get(key);
        cat = bss.getAlControlActionTransactions().get(bss.getAlControlActionTransactions().indexOf(new ControlActionTransaction(ergDeviceId)));
        bOrd = BOrd.make(String.format("local:|station:|%s", key));
        bPoint = bOrd.get().asComponent();
        if(bPoint instanceof BNumericWritable)  
        {
          BNumericWritable bnw = (BNumericWritable) bPoint.asComponent();
          alValidatedResults.add(new ValidatedResults(ergDeviceId, cat.getValue(bnw.getName(), this.getAlDefaultUnitsOfMeasure()), bnw.getLevel(bnw.getActiveLevel()).getValueValue().toString(), bss.getHtErrors().get(ergDeviceId)));
        }
        else if (bPoint instanceof BStringWritable)
        {
          BStringWritable bsw = (BStringWritable) bPoint.asComponent();
          alValidatedResults.add(new ValidatedResults(ergDeviceId, cat.getValue(bsw.getName(), this.getAlDefaultUnitsOfMeasure()), bsw.getLevel(bsw.getActiveLevel()).getValueValue().toString(), bss.getHtErrors().get(ergDeviceId)));
        }
        else if (bPoint instanceof BEnumWritable)
        {
          BEnumWritable bew = (BEnumWritable) bPoint.asComponent();
          alValidatedResults.add(new ValidatedResults(ergDeviceId, cat.getValue(bew.getName(), this.getAlDefaultUnitsOfMeasure()), bew.getLevel(bew.getActiveLevel()).getValueValue().toString(), bss.getHtErrors().get(ergDeviceId)));
        }
        else if (bPoint instanceof BBooleanWritable)
        {
          BBooleanWritable bbw = (BBooleanWritable) bPoint.asComponent();
          alValidatedResults.add(new ValidatedResults(ergDeviceId, cat.getValue(bbw.getName(), this.getAlDefaultUnitsOfMeasure()), bbw.getLevel(bbw.getActiveLevel()).getValueValue().toString(), bss.getHtErrors().get(ergDeviceId)));
        }
        }
        catch(Exception e)
        {
          if(bss.getHtErrors().containsKey(ergDeviceId))
          {  
             alValidatedResults.add(new ValidatedResults(ergDeviceId, "0", "1", bss.getHtErrors().get(ergDeviceId)));
          }   
          else
          {
            alValidatedResults.add(new ValidatedResults(ergDeviceId, "0", "1", String.format("Validation Error ergDevice = %s Error = %s %s", ergDeviceId, e, e.getMessage())));
          }
        }
      }
    }  
  } 
  return new JaceResults(Sys.getStation().getStationName(), alValidatedResults);
}

public Type[] getServiceTypes()
{
  return new Type[] {TYPE};
}

@SuppressWarnings("unchecked")
public BSetSetPointResults doSetSetPoint(BSetSetPointValues setPointValues)
{
   Gson gson = new Gson();
   String parameterName = null;
   Hashtable<String, String> htRemoteChannelIds = new Hashtable<String, String>();
   ControlActionInfo controlActionInfo = new ControlActionInfo();
   String ergDeviceId = null;
   String key = null;
   BOrd bOrd = null;
   BComponent bPoint = null;
   ArrayList<BOrd> alOrds = new ArrayList<BOrd>();
   ArrayList<ControlActionTransaction> alControlActionTransactions = null;
   Hashtable<String, String> htErrors = new Hashtable<String, String>();
   try
   {
      for(Enumeration<String> parameters = setPointValues.getWebop().getRequest().getParameterNames(); parameters.hasMoreElements();)
      {
          parameterName = parameters.nextElement();
          if(parameterName.equals("remoteChannelIds"))
          {
             htRemoteChannelIds = gson.fromJson(setPointValues.getWebop().getRequest().getParameter(parameterName), htRemoteChannelIds.getClass());
          }
          else if(parameterName.equals("controlActionInfo"))
          {
             controlActionInfo = gson.fromJson(setPointValues.getWebop().getRequest().getParameter(parameterName), controlActionInfo.getClass());     
             alControlActionTransactions = controlActionInfo.getAlControlActionTransaction();
          }
      }
      /*
       * If there are htRemoteChannelIds, keyvalue pair of ord and erg_device_id
       */
      if (null != htRemoteChannelIds)
      {
        for (Enumeration<String> keys = htRemoteChannelIds.keys(); keys.hasMoreElements();)
        {
          key = keys.nextElement();
          ergDeviceId = htRemoteChannelIds.get(key);
          try
          {
            ControlActionTransaction cat = controlActionInfo.getAlControlActionTransaction()
                .get(controlActionInfo.getAlControlActionTransaction()
                    .indexOf(new ControlActionTransaction(ergDeviceId)));
            bOrd = BOrd.make(String.format("local:|station:|%s", key));
            bPoint = bOrd.get().asComponent();
            if (bPoint instanceof BNumericWritable)
            {
              BNumericWritable bnw = (BNumericWritable) bPoint.asComponent(); // generate the
                                                                              // component
              alOrds.add(bnw.getAbsoluteOrd()); // add the point to the return structure
              if (cat.getControlActionStatus().equals("REVERT"))
              {  
                bnw.doAuto();
              }   
              else
                bnw.doOverride(new BNumericOverride(Double.valueOf(cat.getValue(bPoint.getName(), this.getAlDefaultUnitsOfMeasure())))); // set the point value
            }
            else if (bPoint instanceof BBooleanWritable) // do not currently have any of these.  // treat as a string
            {
              BBooleanWritable bbw = (BBooleanWritable) bPoint.asComponent(); // generate the component
              alOrds.add(bbw.getAbsoluteOrd()); // add the point to the return structure
            }
            else if (bPoint instanceof BEnumWritable) // do not currently have any of these. treat as a string
            {
              BEnumWritable bew = (BEnumWritable) bPoint.asComponent(); // generate the component
              alOrds.add(bew.getAbsoluteOrd()); // add the point to the return structure
              // bew.doOverride(arg0); //set the point value
            }
            else if (bPoint instanceof BStringWritable)
            {
              BStringWritable bsw = (BStringWritable) bPoint.asComponent(); // generate the component
              alOrds.add(bsw.getAbsoluteOrd()); // add the point to the return structure
              if (cat.getControlActionStatus().equals("REVERT"))
                bsw.doAuto();
              else
                bsw.doOverride(new BStringOverride(cat.getTextValue()));
            }
            htErrors.put(ergDeviceId, "No Error");
          }
          catch (Exception e1)
          {
              htErrors.put(ergDeviceId, String.format("Set Set Point Error ergDeviceId = %s Error = %s %s", ergDeviceId,e1, e1.getMessage()));
          }
        }
      }
    }
    catch(Exception e)
   {
     e.printStackTrace();
   }
   return new BSetSetPointResults(alOrds, alControlActionTransactions, htRemoteChannelIds, htErrors);
}

@Override
public void serviceStarted()
{
  if(((BBoolean)doConfigureModule()).getBoolean())
  {  
     setServletName("accruentSetSetPoints");
     Sys.getStation().save();
  }
  else
  {
    this.setEnabled(false);
    Sys.getStation().save();
    this.stop();
  }
}

public ArrayList<String> getAlDefaultUnitsOfMeasure()
{
  return alDefaultUnitsOfMeasure;
}

public void setAlDefaultUnitsOfMeasure(ArrayList<String> alDefaultUnitsOfMeasure)
{
  this.alDefaultUnitsOfMeasure = alDefaultUnitsOfMeasure;
}

class JaceResults
{
  String siteName = null;
  ArrayList<ValidatedResults> alValidatedResults = null;
  JaceResults(String siteNamep, ArrayList<ValidatedResults> alValidatedResultsp)
  {
    this.siteName = siteNamep;
    this.alValidatedResults = alValidatedResultsp;
  }
  
}
class ValidatedResults
{
   String ergDeviceId = null;
   String actionedValue = null;
   String deviceValue = null;
   String error = null;
   ValidatedResults(String ergDeviceIdp, String actionedValuep, String deviceValuep, String errorp)
   {
     this.ergDeviceId = ergDeviceIdp;
     this.actionedValue = actionedValuep;
     this.deviceValue = deviceValuep;
     this.error = errorp;
   }
}

}
