package com.accruent.setsetpoint.controlaction;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ControlActionTransaction implements Serializable
{
    /**
   * 
   */
  private static final long serialVersionUID = 1L;
  String controlActionId = null;
    String controlActionResultId = null;
    String controllerName = null;
    String controllerId = null;  // used in test bed to get the controller from the slave list
    String channelName = null;
    String actionedValue = null;
    String previousValue = null;
    int controlActionStatusId = 0;
    int revertActionStatusId = 0;
    String energySchema = null;
    String ergDeviceId = null;
    String ergSystemVariableId = null;
    String cemUnitId = null;
    String multiplier = null;
    boolean isGoodUpdate = true;
    String conversionFactorToDefault = null;
    String conversionAddendToDefault = null;
    String deviceUom = null;
    String textValue = null;
    String cemUnitAbbreviation = null;
    String revertTextValue = null;
    String ergControllerDeviceId = null;
    String textValueName = null;

    public ControlActionTransaction(String controlActionIdp, String controlActionResultIdp, String controllerNamep, String controllerIdp, String channelNamep, String actionedValuep, String previousValuep, int controlActionStatusIdp, int revertActionStatusIdp, String energySchemap, String ergDeviceIdp, String ergSystemVariableIdp, String cemUnitIdp, String multiplierp, String conversionFactorToDefaultp, String conversionAddendToDefaultp, String deviceUomp, String textValuep, String cemUnitAbbreviationp, String revertTextValuep, String ergControllerDeviceIdp, String textValueNamep)
    {
        this.controlActionId = controlActionIdp;
        this.controlActionResultId = controlActionResultIdp;
        this.controllerName = controllerNamep;
        this.controllerId = controllerIdp;
        this.channelName = channelNamep;
        this.actionedValue = actionedValuep;
        this.previousValue = previousValuep;
        this.controlActionStatusId = controlActionStatusIdp;
        this.revertActionStatusId = revertActionStatusIdp;
        this.energySchema = energySchemap;
        this.ergDeviceId = ergDeviceIdp;
        this.ergSystemVariableId = ergSystemVariableIdp;
        this.cemUnitId = cemUnitIdp;
        this.multiplier = multiplierp;
        this.conversionFactorToDefault = conversionFactorToDefaultp;
        this.conversionAddendToDefault = conversionAddendToDefaultp;
        this.deviceUom = deviceUomp;
        this.textValue = textValuep;
        this.cemUnitAbbreviation = cemUnitAbbreviationp;
        this.revertTextValue = revertTextValuep;
        this.ergControllerDeviceId = ergControllerDeviceIdp;
        this.textValueName = textValueNamep;
    }
    
    public ControlActionTransaction(String ergDeviceIdp)
    {
      this.ergDeviceId = ergDeviceIdp;
    }

    public String getControlActionId()
    {
        return controlActionId;
    }

    public void setControlActionId(String controlActionId)
    {
        this.controlActionId = controlActionId;
    }

    public String getControlActionResultId()
    {
        return controlActionResultId;
    }

    public void setControlActionResultId(String controlActionResultId)
    {
        this.controlActionResultId = controlActionResultId;
    }

    public String getControllerName()
    {
        return controllerName;
    }

    public void setControllerName(String controllerName)
    {
        this.controllerName = controllerName;
    }

    public String getChannelName()
    {
        return channelName;
    }

    public String getControllerId()
    {
        return controllerId;
    }

    public void setControllerId(String controllerId)
    {
        this.controllerId = controllerId;
    }

    public void setChannelName(String channelName)
    {
        this.channelName = channelName;
    }

    public String getActionedValue()
    {
        return actionedValue;
    }

    public void setActionedValue(String actionedValue)
    {
        this.actionedValue = actionedValue;
    }

    public String getPreviousValue()
    {
        return previousValue;
    }

    public void setPreviousValue(String previousValue)
    {
        this.previousValue = previousValue;
    }

    public int getControlActionStatusId()
    {
        return controlActionStatusId;
    }

    public void setControlActionStatusId(int controlActionStatusId)
    {
        this.controlActionStatusId = controlActionStatusId;
    }

    public int getRevertActionStatusId()
    {
        return revertActionStatusId;
    }

    public void setRevertActionStatusId(int reverActionStatusId)
    {
        this.revertActionStatusId = reverActionStatusId;
    }

    public String getValue(String name, ArrayList<String> alDeviceUnitsOfMeasure)
    {
        DecimalFormat df = new DecimalFormat("####.#");
        String value = null;
        if(this.getRevertActionStatusId() == 4) // revert not scheduled use action
        {
            value = this.getActionedValue();
        }
        else if(this.getControlActionStatusId() == 4) //action not scheduled use revert
        {
            value = this.getPreviousValue();
        }
        else if(this.getControlActionStatusId() == 1 && this.getRevertActionStatusId() == 1) //both are scheduled do action first
        {
            value = this.getActionedValue();
        }
        else
        {
            value = this.getPreviousValue(); // if we get here it must be a revert?
        }
        //lets see if we can format this to one decimal place if it is numeric
        if (null != this.getCemUnitId())
        {
            if(this.getCemUnitId().equals("1002")) //minutes:
            {  
                value = String.valueOf(df.format(Math.round(Float.parseFloat(value.trim()) / 0.0166667)));
            }    
            else
            {  
                if (alDeviceUnitsOfMeasure.contains(this.getDeviceUom()) && this.getDeviceUom().equals(this.getCemUnitId())) //process normally if we are in the client selected unit of measure...
                {
                    try
                    {
                        value = String.valueOf(df.format(Float.parseFloat(value.trim())));
                    }
                    catch (NullPointerException npe)
                    {
                        value = "";
                    }
                    catch (NumberFormatException nfe)
                    {
                        value = "nfe";
                    }
                }
                else //we need to convert from our default units to the devices units.
                {
                        if (this.getDeviceUom().equals("1101")) //ic.getAppGlobalDatum("CEM_UNITS:CELSIUS"))) // convert from F to C for the device
                        {
                            try
                            {
                                value = String.valueOf(df.format((Float.parseFloat(value.trim()) - 32) * 5 / 9));
                            }
                            catch (NullPointerException npe)
                            {
                                value = "";
                            }
                            catch (NumberFormatException nfe)
                            {
                                value = "nfe";
                            }
                        }
                        else if (this.getDeviceUom().equals("1102")) //ic.getAppGlobalDatum("CEM_UNITS:FAHRENHEIT"))) // convert from C to F for the device
                        {
                            try
                            {
                                value = String.valueOf(df.format((Float.parseFloat(value.trim()) * 9 / 5) + 32));
                            }
                            catch (NullPointerException npe)
                            {
                                value = "";
                            }
                            catch (NumberFormatException nfe)
                            {
                                value = "nfe";
                            }
                        }
                        else if(this.getDeviceUom().equals("1001")) //ic.getAppGlobalDatum("CEM_UNITS:SECONDS"))) //convert from minutes to seconds for the device
                        {
                            try
                            {
                                value = String.valueOf(df.format(Math.round(Float.parseFloat(value.trim()) / 0.0166667)));
                            }
                            catch(NullPointerException npe)
                            {
                                value = "";
                            }
                            catch(NumberFormatException nfe)
                            {
                                value = "nfe";
                            }
                        }
                        else if(this.getDeviceUom().equals("1002")) //ic.getAppGlobalDatum("CEM_UNITS:MINUTES"))) //convert from seconds to minutes for the device
                        {
                            try
                            {
                                value = String.valueOf(df.format(Float.parseFloat(value.trim()) * 0.0166667 ));
                            }
                            catch(NullPointerException npe)
                            {
                                value = "";
                            }
                            catch(NumberFormatException nfe)
                            {
                                value = "nfe";
                            }
                        }
                }
            }
        }
        return value;
    }


    public String getValue(boolean isText)
    {
        String value = null;
        if(this.getRevertActionStatusId() == 4) // revert not scheduled use action
        {
            value = this.getTextValue();
        }
        else if(this.getControlActionStatusId() == 4) //action not scheduled use revert
        {
            value = this.getRevertTextValue();
        }
        else if(this.getControlActionStatusId() == 1 && this.getRevertActionStatusId() == 1) //both are scheduled do action first
        {
            value = this.getTextValue();
        }
        else
        {
            value = this.getRevertTextValue(); // if we get here it must be a revert?
        }
        return value;
    }
    
    public String getValue()
    {
        String value = null;
        if(this.getRevertActionStatusId() == 4) // revert not scheduled use action
        {
            value = this.getActionedValue();
        }
        else if(this.getControlActionStatusId() == 4) //action not scheduled use revert
        {
            value = this.getPreviousValue();
        }
        else if(this.getControlActionStatusId() == 1 && this.getRevertActionStatusId() == 1) //both are scheduled do action first
        {
            value = this.getActionedValue();
        }
        else
        {
            value = this.getPreviousValue(); // if we get here it must be a revert?
        }
        //lets see if we can format this to one decimal place if it is numeric
        try
        {
            value = String.valueOf(Float.parseFloat(value.trim()));
        }
        catch (NullPointerException npe)
        {
            value = "";
        }
        catch (NumberFormatException nfe)
        {
            value = "nfe";
        }
        return value;
    }



    public String getEnergySchema()
    {
        return energySchema;
    }

    public void setEnergySchema(String energySchema)
    {
        this.energySchema = energySchema;
    }

    public String getErgDeviceId()
    {
        return ergDeviceId;
    }

    public void setErgDeviceId(String ergDeviceId)
    {
        this.ergDeviceId = ergDeviceId;
    }

    public String getErgSystemVariableId()
    {
        return ergSystemVariableId;
    }

    public void setErgSystemVariableId(String ergSystemVariableId)
    {
        this.ergSystemVariableId = ergSystemVariableId;
    }

    public String getCemUnitId()
    {
        return cemUnitId;
    }

    public void setCemUnitId(String cemUnitId)
    {
        this.cemUnitId = cemUnitId;
    }

    public String getMultiplier()
    {
        return multiplier;
    }

    public void setMultiplier(String multiplier)
    {
        this.multiplier = multiplier;
    }

    public boolean isGoodUpdate()
    {
        return isGoodUpdate;
    }

    public void setGoodUpdate(boolean isGoodUpdate)
    {
        this.isGoodUpdate = isGoodUpdate;
    }
    public String getControlActionStatus()
    {
        String controlActionStatus = null;
        if(this.getRevertActionStatusId() == 4) // revert not scheduled use action
        {
            controlActionStatus = "ACTION";
        }
        else if(this.getControlActionStatusId() == 4) //action not scheduled use revert
        {
            controlActionStatus = "REVERT";
        }
        else if(this.getControlActionStatusId() == 1 && this.getRevertActionStatusId() == 1) //both are scheduled do action first
        {
            controlActionStatus = "ACTION";
        }
        else if(this.getControlActionStatusId() == 1)  //pending action
        {
            controlActionStatus = "ACTION";
        }
        else if(this.getRevertActionStatusId() == 1)  //pending revert
        {
            controlActionStatus = "REVERT";
        }
        else
        {
            controlActionStatus = "ACTION";  // if we get here it must be a revert?
        }

        return controlActionStatus;
    }

    public String getConversionFactorToDefault()
    {
        return conversionFactorToDefault;
    }

    public void setConversionFactorToDefault(String conversionFactorToDefault)
    {
        this.conversionFactorToDefault = conversionFactorToDefault;
    }

    public String getConversionAddendToDefault()
    {
        return conversionAddendToDefault;
    }

    public void setConversionAddendToDefault(String conversionAddendToDefault)
    {
        this.conversionAddendToDefault = conversionAddendToDefault;
    }

    public String getDeviceUom()
    {
        return deviceUom;
    }

    public void setDeviceUom(String deviceUom)
    {
        this.deviceUom = deviceUom;
    }

    public String getTextValue()
    {
        return textValue;
    }

    public void setTextValue(String textValue)
    {
        this.textValue = textValue;
    }

    public String getCemUnitAbbreviation()
    {
        return cemUnitAbbreviation;
    }

    @Override
    public int hashCode()
    {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((ergDeviceId == null) ? 0 : ergDeviceId.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj)
    {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      ControlActionTransaction other = (ControlActionTransaction) obj;
      if (ergDeviceId == null)
      {
        if (other.ergDeviceId != null)
          return false;
      }
      else if (!ergDeviceId.equals(other.ergDeviceId))
        return false;
      return true;
    }

    public void setCemUnitAbbreviation(String cemUnitAbbreviation)
    {
        this.cemUnitAbbreviation = cemUnitAbbreviation;
    }

    public String getRevertTextValue()
    {
        return revertTextValue;
    }

    public void setRevertTextValue(String revertTextValue)
    {
        this.revertTextValue = revertTextValue;
    }

    public String getTextValueName()
    {
        return textValueName;
    }

    public void setTextValueName(String textValueName)
    {
        this.textValueName = textValueName;
    }

}
