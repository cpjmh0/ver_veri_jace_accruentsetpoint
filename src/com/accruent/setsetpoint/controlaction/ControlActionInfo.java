package com.accruent.setsetpoint.controlaction;

import java.io.Serializable;
import java.util.ArrayList;

public class ControlActionInfo implements Serializable
{
    /**
   * 
   */
  private static final long serialVersionUID = 1L;
  String siteId = null;
    String siteName = null;
    String clientName = null;
    String url = null;
    String login = null;
    String password = null;
    String ergControllerModel = null;
    ArrayList<ControlActionTransaction> alControlActionTransaction = null;
    ArrayList<String> alControlActionIds = null;
    int controlActionStatusId = 0;
    int revertActionStatusId = 0;
    int port = 0;

    public ControlActionInfo(String siteIdp, String siteNamep, String urlp, String loginp, String passwordp, String ergControllerModelp, ArrayList<ControlActionTransaction> alControlActionTransactionp, int controlActionStatusIdp, int revertActionStatusIdp, String clientNamep, int portp)
    {
        this.siteId = siteIdp;
        this.siteName = siteNamep;
        this.url = urlp;
        this.login = loginp;
        this.password = passwordp;
        this.ergControllerModel = ergControllerModelp;
        this.alControlActionTransaction = alControlActionTransactionp;
        this.controlActionStatusId = controlActionStatusIdp;
        this.revertActionStatusId = revertActionStatusIdp;
        this.clientName = clientNamep;
        this.port = portp;
    }
    public ControlActionInfo(ArrayList<String> alControlActionIdsp)
    {
        this.alControlActionIds = alControlActionIdsp;
    }

    public ControlActionInfo()
    {
      
    }
    public String getSiteId()
    {
        return siteId;
    }

    public void setSiteId(String siteId)
    {
        this.siteId = siteId;
    }

    public String getSiteName()
    {
        return siteName;
    }

    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getErgControllerModel()
    {
        return ergControllerModel;
    }

    public void setErgControllerModel(String ergControllerModel)
    {
        this.ergControllerModel = ergControllerModel;
    }

    public ArrayList<ControlActionTransaction> getAlControlActionTransaction()
    {
        return alControlActionTransaction;
    }

    public void setAlControlActionTransaction(ArrayList<ControlActionTransaction> alControlActionTransaction)
    {
        this.alControlActionTransaction = alControlActionTransaction;
    }

    public ArrayList<String> getAlControlActionIds()
    {
        return alControlActionIds;
    }

    public void setAlControlActionIds(ArrayList<String> alControlActionIds)
    {
        this.alControlActionIds = alControlActionIds;
    }
    public int getControlActionStatusId()
    {
        return controlActionStatusId;
    }
    public void setControlActionStatusId(int controlActionStatusId)
    {
        this.controlActionStatusId = controlActionStatusId;
    }
    public String getControlActionStatus()
    {
        String controlActionStatus = null;
        if(this.getRevertActionStatusId() == 4) // revert not scheduled use action
        {
            controlActionStatus = "ACTION";
        }
        else if(this.getControlActionStatusId() == 4) //action not scheduled use revert
        {
            controlActionStatus = "REVERT";
        }
        else if(this.getControlActionStatusId() == 1 && this.getRevertActionStatusId() == 1) //both are scheduled do action first
        {
            controlActionStatus = "ACTION";
        }
        else
        {
            controlActionStatus = "REVERT";  // if we get here it must be a revert?
        }

        return controlActionStatus;
    }
    public int getRevertActionStatusId()
    {
        return revertActionStatusId;
    }
    public void setRevertActionStatusId(int revertActionStatusId)
    {
        this.revertActionStatusId = revertActionStatusId;
    }
    public String getClientName()
    {
        return clientName;
    }
    public void setClientName(String clientName)
    {
        this.clientName = clientName;
    }
    public int getPort()
    {
        return port;
    }
    public void setPort(int port)
    {
        this.port = port;
    }
}

